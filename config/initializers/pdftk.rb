platform = RUBY_PLATFORM

if platform.include?("darwin") # OS X machine
  binary_path = '/usr/local/Cellar/pdftk/2.02/bin/pdftk'
elsif platform.include?("64-linux") # 64-bit linux machine
  binary_path = Rails.root.join('vendor/pdftk/bin', 'pdftk').to_s
end

PDFTK_PATH = binary_path