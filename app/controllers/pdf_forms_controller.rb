# require '/lib/pdfs/test.pdf.form.rb'
class PdfFormsController < ApplicationController
  # before_action :set_pdf_form, only: [:show, :edit, :update, :destroy]
  
  # GET /pdf_forms
  # GET /pdf_forms.json
  def index
    
    # render :pdf => TestPdfForm.new.export,type: 'application/pdf'
    respond_to do |format|
      format.pdf {send_file TestPdfForm.new.export,type: 'application/pdf', disposition: 'inline'}  
    end
    # a = TestPdfForm.new
    # render json: {field_names:a}
  end

  # GET /pdf_forms/1
  # GET /pdf_forms/1.json
  def show
  end

  # GET /pdf_forms/new
  def new
    @pdf_form = PdfForm.new
  end

  # GET /pdf_forms/1/edit
  def edit
  end

  # POST /pdf_forms
  # POST /pdf_forms.json
  def create
    @pdf_form = PdfForm.new(pdf_form_params)

    respond_to do |format|
      if @pdf_form.save
        format.html { redirect_to @pdf_form, notice: 'Pdf form was successfully created.' }
        format.json { render :show, status: :created, location: @pdf_form }
      else
        format.html { render :new }
        format.json { render json: @pdf_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pdf_forms/1
  # PATCH/PUT /pdf_forms/1.json
  def update
    respond_to do |format|
      if @pdf_form.update(pdf_form_params)
        format.html { redirect_to @pdf_form, notice: 'Pdf form was successfully updated.' }
        format.json { render :show, status: :ok, location: @pdf_form }
      else
        format.html { render :edit }
        format.json { render json: @pdf_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pdf_forms/1
  # DELETE /pdf_forms/1.json
  def destroy
    @pdf_form.destroy
    respond_to do |format|
      format.html { redirect_to pdf_forms_url, notice: 'Pdf form was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pdf_form
      @pdf_form = PdfForm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pdf_form_params
      params[:pdf_form]
    end
end
