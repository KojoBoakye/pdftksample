class TestPdfForm < FillablePdfForm
		
			def initialize
				fill_out
			end
			
			def fill_out
				fill :customer_name, "Ernest Kobi adjei"
				fill :address, "Box 224466"
				fill :date_day, Date.today.mday.to_s
				fill :date_month, Date.today.mon.to_s
				fill :date_year, Date.today.year.to_s
				products.each do |p|
					p.each { |k, v| fill k, v}
				end
					
				# products.each { |k, v| fill k, v}
			end
			
			def products
				# [{"product_0" => "Kraft Liner","quantity_0" => 10,"price_0" => 100,"amount_0" => 100.00}]
				 [{
					"product_0" => "Kraft Liner","quantity_0" => 10,"price_0" => 100,"amount_0" => 100.00
				},{
					"product_1" => "Kraft Liner","quantity_1" => 10,"price_1" => 100,"amount_1" => 100.00
				},{
					"product_2" => "Kraft Liner 2","quantity_2" => 100,"price_2" => 1000,"amount_2" => 1000.00
				},{
					"product_3" => "Kraft Liner 3","quantity_3" => 20,"price_3" => 50,"amount_3" => 500.00
				}
				]
			end
	
end