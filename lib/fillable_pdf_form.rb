require 'pdf_forms'

# temp implementation
# pdftk = PdfForms.new('/usr/local/Cellar/pdftk/2.02/bin/pdftk') 
# field_names =  pdftk.get_field_names 'lib/pdf_templates/Fon Invoice Form.pdf'

class FillablePdfForm
	
	attr_writer :template_path
	attr_reader :attributes
	
	def initializer
		fill_out
	end
	
	def export(output_file_path=nil)
		
    output_path = output_file_path || "#{Rails.root}/tmp/pdfs/#{SecureRandom.uuid}.pdf" # make sure tmp/pdfs exists
    pdftk.fill_form template_path, output_path, attributes, :flatten => true
    output_path
		
  end
	
	def show_field_names
		# puts @attributes
		dump_data_fields
	end
	
	def get_field_names
		pdftk.get_field_names template_path
	end
	
	
	def template_path
			 @template_path = 'lib/pdf_templates/Fon_Invoice_Form.pdf'
			 # @template_path ||= "#{Rails.root}/lib/pdf_templates/#{self.class.name.gsub('Pdf', '').underscore}.pdf"
			 # makes assumption about template file path unless otherwise specified
	end
	
	
	def attributes
		@attributes ||= {}
	end
	
	def fill(key,value)
		attributes[key.to_s] = value
	end
	
	def pdftk
   	# PdfForms.new(ENV['PDFTK_PATH'] || '/usr/local/Cellar/pdftk/2.02/bin/pdftk')
		PdfForms.new(PDFTK_PATH)  
		# On my Mac, the location of pdftk was different than on my linux server.
  end
	
	def fill_out
    raise 'Must be overridden by child class'
  end
	
	
end