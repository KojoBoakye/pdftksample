require 'test_helper'

class PdfFormsControllerTest < ActionController::TestCase
  setup do
    @pdf_form = pdf_forms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pdf_forms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pdf_form" do
    assert_difference('PdfForm.count') do
      post :create, pdf_form: {  }
    end

    assert_redirected_to pdf_form_path(assigns(:pdf_form))
  end

  test "should show pdf_form" do
    get :show, id: @pdf_form
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pdf_form
    assert_response :success
  end

  test "should update pdf_form" do
    patch :update, id: @pdf_form, pdf_form: {  }
    assert_redirected_to pdf_form_path(assigns(:pdf_form))
  end

  test "should destroy pdf_form" do
    assert_difference('PdfForm.count', -1) do
      delete :destroy, id: @pdf_form
    end

    assert_redirected_to pdf_forms_path
  end
end
